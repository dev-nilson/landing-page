import { useRef } from "react";
import bannerTop from "../../assets/banner-top.png";
import bannerBottom from "../../assets/banner-bottom.png";
import "./Slider.scss";

function Slider() {
  const sliderTopRef = useRef<HTMLDivElement>(null);

  const slide = (e: React.MouseEvent<HTMLDivElement>) => {
    if (sliderTopRef.current) {
      sliderTopRef.current.style.width = `${e.clientX}px`;
    }
  };

  return (
    <div className="slider" onMouseMove={slide}>
      <div className="slider__panel">
        <div>
          <h1 className="slider__title">Expand your business</h1>
          <p className="slider__subtitle">Reach spanish-speaking customers</p>
        </div>
        <div>
          <img src={bannerTop} />
        </div>
      </div>
      <div className="slider__top" ref={sliderTopRef}>
        <div className="slider__panel">
          <div>
            <h1 className="slider__title">Expand your business</h1>
            <p className="slider__subtitle">Reach spanish-speaking customers</p>
          </div>
          <div>
            <img src={bannerBottom} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Slider;
