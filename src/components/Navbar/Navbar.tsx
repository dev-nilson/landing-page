import Button from "../Button/Button";
import "./Navbar.scss";

function Navbar() {
  return (
    <div className="navbar">
      <div className="navbar__container">
        <div>Logo</div>
        <div>
          <ul className="navbar__list">
            <li>
              <a className="navbar__link" href="#">
                About
              </a>
            </li>
            <li>
              <a className="navbar__link" href="#">
                Pricing
              </a>
            </li>
            <li>
              <a className="navbar__link" href="#">
                FAQs
              </a>
            </li>
            <li>
              <Button label="Contact Us" />
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
