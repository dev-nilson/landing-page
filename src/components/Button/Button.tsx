import { ButtonProps } from "../../types/types";
import "./Button.scss";

function Button({ label }: ButtonProps) {
  return <button className="button">{label}</button>;
}

export default Button;
