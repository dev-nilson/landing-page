import Slider from "./components/Slider/Slider";
import "./App.scss";
import Navbar from "./components/Navbar/Navbar";

function App() {
  return (
    <div className="app">
      <Navbar />
      <Slider />
    </div>
  );
}

export default App;
